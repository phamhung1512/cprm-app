-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Máy chủ: localhost
-- Thời gian đã tạo: Th10 14, 2017 lúc 04:45 CH
-- Phiên bản máy phục vụ: 10.1.21-MariaDB
-- Phiên bản PHP: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `cprm`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `address_supplier`
--

CREATE TABLE `address_supplier` (
  `id` int(11) NOT NULL,
  `place` varchar(255) CHARACTER SET utf8 COLLATE utf8_vietnamese_ci NOT NULL,
  `address1` varchar(255) CHARACTER SET utf8 COLLATE utf8_vietnamese_ci NOT NULL,
  `address2` varchar(255) CHARACTER SET utf8 COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `phone_number` int(11) DEFAULT NULL,
  `zip_code` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `district_id` int(11) DEFAULT NULL,
  `supplier_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `address_supplier`
--

INSERT INTO `address_supplier` (`id`, `place`, `address1`, `address2`, `email`, `phone_number`, `zip_code`, `country_id`, `city_id`, `district_id`, `supplier_id`) VALUES
(14, 'HCM', 'Q.1', '', 'asus@gmail.com', 258456123, NULL, NULL, NULL, NULL, 'SUP31051786'),
(15, 'HCM', 'Q2', '', '', NULL, NULL, NULL, NULL, NULL, 'SUP310517603');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `brand`
--

CREATE TABLE `brand` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_vietnamese_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `brand`
--

INSERT INTO `brand` (`id`, `name`) VALUES
(1, 'Sony'),
(2, 'Samsung'),
(3, 'Nokia'),
(4, 'Apple'),
(5, 'Iphone');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_vietnamese_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `category`
--

INSERT INTO `category` (`id`, `name`, `description`) VALUES
(1, 'Smart Phone', NULL),
(2, 'TV', NULL),
(3, 'Laptop', NULL),
(4, 'PC', NULL),
(5, 'Console', NULL),
(6, 'Ear Phone', NULL),
(7, 'Head Phone', NULL),
(8, 'Mice', NULL),
(9, 'Keyboard', NULL),
(10, 'Screen', NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `city`
--

CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_vietnamese_ci NOT NULL,
  `country_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `city`
--

INSERT INTO `city` (`id`, `name`, `country_id`) VALUES
(1, 'Hồ Chí Minh', 1),
(2, 'Hà Nội', 1),
(3, 'Đà Nẵng', 1),
(4, 'Bangkok', 2),
(5, 'Phonompep', 3);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `country`
--

CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_vietnamese_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `country`
--

INSERT INTO `country` (`id`, `name`) VALUES
(1, 'Việt Nam'),
(2, 'Thái Lan'),
(3, 'Lào'),
(4, 'Campuchia'),
(5, 'Malaysia');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `currency`
--

CREATE TABLE `currency` (
  `id` varchar(255) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_vietnamese_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `currency`
--

INSERT INTO `currency` (`id`, `name`) VALUES
('VND', 'Việt Nam Đồng');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `name` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `gender` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthdate` date DEFAULT NULL,
  `description` text,
  `fax` int(11) DEFAULT NULL,
  `tax_code` int(11) DEFAULT NULL,
  `website` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `IsActive` int(11) NOT NULL DEFAULT '0',
  `group_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `customer`
--

INSERT INTO `customer` (`id`, `name`, `code`, `phone`, `email`, `gender`, `birthdate`, `description`, `fax`, `tax_code`, `website`, `created_at`, `IsActive`, `group_id`) VALUES
(1, 'Huy', 'CUS1231233', '0966566777', 'huytrinhquoc93@gmail.com', 'male', '2017-05-23', '', NULL, NULL, '', NULL, 0, 1),
(4, 'Toàn', 'CUS0000005', '0966008349', '', 'male', NULL, '', NULL, NULL, '', '2017-05-29 11:39:48', 1, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `customer_address`
--

CREATE TABLE `customer_address` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `place` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `address1` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `address2` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` int(11) DEFAULT NULL,
  `ZIP_code` int(11) DEFAULT NULL,
  `country` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `districts` varchar(256) CHARACTER SET utf32 COLLATE utf32_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `customer_address`
--

INSERT INTO `customer_address` (`id`, `customer_id`, `place`, `address1`, `address2`, `email`, `phone`, `ZIP_code`, `country`, `city`, `districts`, `created_at`) VALUES
(1, 1, 'Nhà', 'Hồ Chí Minh', '', '', NULL, NULL, 'Việt Nam', '', '', '2017-05-29 11:19:07'),
(2, 4, 'Nhà', '65 Pham Ngoc Thach Quận 3, Thành Phố Hồ Chí Minh', '', '', NULL, NULL, 'Việt Nam', '', '', '2017-05-29 11:39:48'),
(4, 1, 'abcvkl', 'Mình Chí Hô', '', '', NULL, NULL, 'Việt Nam', '', '', '2017-05-30 17:07:09'),
(5, 1, 'Nhà riêng', '123 Hồ Thị Kỷ', '', '', NULL, NULL, '', '', '', '2017-06-05 14:09:54');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `customer_contact`
--

CREATE TABLE `customer_contact` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `name` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `position` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `department` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` int(11) DEFAULT NULL,
  `fax` int(11) DEFAULT NULL,
  `mobile_number` int(11) DEFAULT NULL,
  `note` text,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `customer_contact`
--

INSERT INTO `customer_contact` (`id`, `customer_id`, `name`, `email`, `position`, `department`, `phone`, `fax`, `mobile_number`, `note`, `created_at`) VALUES
(1, 1, 'abc', 'abc@gmail.com', '', '', 1234567890, NULL, NULL, '', NULL),
(3, 1, 'huy', 'huy@gmail.com', '', '', 966008349, NULL, 966008349, '', '2017-05-30 17:17:21');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `customer_default_setting`
--

CREATE TABLE `customer_default_setting` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `group_price_id` int(11) DEFAULT NULL,
  `group_tax_id` int(11) DEFAULT NULL,
  `discount` double DEFAULT NULL,
  `term_of_payment_id` int(11) DEFAULT NULL,
  `group_payment` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `customer_default_setting`
--

INSERT INTO `customer_default_setting` (`id`, `customer_id`, `employee_id`, `group_price_id`, `group_tax_id`, `discount`, `term_of_payment_id`, `group_payment`) VALUES
(1, 1, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 4, 6, 2, 2, 0, 1, 4);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `customer_note`
--

CREATE TABLE `customer_note` (
  `id` int(11) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `note` text NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `customer_note`
--

INSERT INTO `customer_note` (`id`, `customer_id`, `note`, `user_id`, `created_at`) VALUES
(1, 1, 'haha', NULL, NULL),
(3, 1, 'abcmello', NULL, '2017-06-05 16:19:19'),
(4, 1, 'abcvkl', NULL, '2017-06-05 16:37:32');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `district`
--

CREATE TABLE `district` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_vietnamese_ci NOT NULL,
  `city_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `district`
--

INSERT INTO `district` (`id`, `name`, `city_id`) VALUES
(1, 'Quận 1', 1),
(2, 'Quận 2', 1),
(3, 'Quận 3', 1),
(4, 'Quận 4', 1),
(5, 'XYZ', 4);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `employee`
--

CREATE TABLE `employee` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_vietnamese_ci NOT NULL,
  `phone_number` int(11) NOT NULL,
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `birthday` datetime DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `note` text CHARACTER SET utf8 COLLATE utf8_vietnamese_ci,
  `status` int(11) NOT NULL,
  `password_hash` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `employee`
--

INSERT INTO `employee` (`id`, `name`, `phone_number`, `address`, `birthday`, `email`, `note`, `status`, `password_hash`, `created_at`) VALUES
(4, 'Messi', 123456, 'a', '2017-05-07 00:00:00', 'asd@gmail.com', 'asdasdzxczxczxc', 1, '$2y$13$YyM3FFfngFI1KMVc/zUHeewdA7dRYUvg3RQ1M6XVm4vRCPY2OC8Bi', '2017-05-12 14:20:30'),
(5, 'Messi', 123456, 'asd', '2017-05-01 00:00:00', 'messi@gmail.com', 'ahjkhjkh', 2, '$2y$13$OZA9c.I6CAKN5w1iukclCeNIt.159IjzUBYtDEhcR7VoiU8S6n4Zq', '2017-05-12 14:32:07'),
(6, 'Messi', 123456, 'a', '2017-05-01 00:00:00', 'cr7@gmail.com', 'a', 2, '$2y$13$KAXu6ziXEayQ3HhiqMQ0OOG1JoyRrtgJI0vTeTYGfA0AyInhDNdMS', '2017-05-12 15:36:39'),
(9, '12313', 1231233, '', '2017-05-30 00:00:00', 'qweq@dasda.com', '', 0, '$2y$13$NM0vSdikIilsKqaP7HFMWuSTnGk0t4TEUu.ghJJkD4j8u/Y9vYKE.', '2017-05-31 09:27:34'),
(10, '213123', 123123123, '', '2017-05-09 00:00:00', 'qweq123@dasda.com', '', 0, '$2y$13$Fbubj070x8KZ620WQQGLf.3WQLb6ruGCNxtM9HvnooQPaWOcWXEfi', '2017-05-31 09:28:30'),
(11, 'wqeqweqwe', 2147483647, 'qweqweqwe', '2017-05-23 00:00:00', 'qweq123213@dasda.com', '', 0, '$2y$13$3xCZuxuPQ1EHHdsvfznUVOkDw2Dn/1WM/GEggzXN69w3PCBvN8mQW', '2017-05-31 09:29:32'),
(12, '12312312', 123456, '', '2017-05-16 00:00:00', 'asd123@gmail.com', '', 0, '$2y$13$4HmGkxG7a8.M16Hjlbg2LuqPppZjFDJuFAnGK.Sm0/lco5lIlVs9K', '2017-05-31 09:34:17'),
(13, 'qweqweqwe', 2147483647, '', '2017-05-15 00:00:00', 'qweqrwew@dasda.com', '', 0, '$2y$13$Rk2aXS0ZRzBi8g18BLltPufCUHYlbiAAEyFO6gh7xQVzfkXFKvgeu', '2017-05-31 09:37:54');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `group_customer`
--

CREATE TABLE `group_customer` (
  `id` int(11) NOT NULL,
  `name` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `IsActive` int(11) DEFAULT '1',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `group_customer`
--

INSERT INTO `group_customer` (`id`, `name`, `IsActive`, `created_at`) VALUES
(1, 'VIP', 1, '2017-05-15 14:44:54'),
(2, 'Thường', 1, NULL),
(3, 'Thường Xuyên', 1, NULL),
(4, 'huy', 1, NULL),
(5, 'HAY QUA', 1, NULL),
(6, 'Qua VIP', 1, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `group_payment`
--

CREATE TABLE `group_payment` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_vietnamese_ci NOT NULL,
  `kind_of_payment` int(11) NOT NULL,
  `is_default` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `group_payment`
--

INSERT INTO `group_payment` (`id`, `name`, `kind_of_payment`, `is_default`) VALUES
(1, 'Chuyển khoản', 1, 0),
(2, 'COD', 2, 0),
(3, 'Thanh toán bằng điểm', 4, 0),
(4, 'Tiền mặt', 0, 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `group_price`
--

CREATE TABLE `group_price` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `currency_id` varchar(255) NOT NULL,
  `kind_of_price` varchar(255) CHARACTER SET utf8 COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `is_inputprice` int(11) DEFAULT NULL,
  `is_default` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `group_price`
--

INSERT INTO `group_price` (`id`, `name`, `currency_id`, `kind_of_price`, `is_inputprice`, `is_default`) VALUES
(1, 'Giá bán buôn', 'VND', '	Bán hàng', 0, 0),
(2, 'Giá bán lẻ', 'VND', '	Bán hàng', 0, 1),
(3, 'Giá nhập', 'VND', 'Nhập hàng', 1, 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `group_supplier`
--

CREATE TABLE `group_supplier` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `group_supplier`
--

INSERT INTO `group_supplier` (`id`, `name`) VALUES
(1, 'VIP'),
(2, 'Normal');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `group_tax`
--

CREATE TABLE `group_tax` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `percent` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `group_tax`
--

INSERT INTO `group_tax` (`id`, `name`, `percent`) VALUES
(1, 'Thuế bán hàng', 10),
(2, 'Không áp dụng thuế', 0),
(3, 'Thuế nhập hàng', 10);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `import_bill`
--

CREATE TABLE `import_bill` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `term_of_payment_id` int(11) DEFAULT NULL,
  `time_to_payment` datetime DEFAULT NULL,
  `reference` varchar(255) DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `product_price` decimal(10,2) DEFAULT NULL,
  `VAT` decimal(10,2) DEFAULT NULL,
  `total_price` decimal(10,2) DEFAULT NULL,
  `note` varchar(255) DEFAULT NULL,
  `purchase_order_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `import_bill`
--

INSERT INTO `import_bill` (`id`, `name`, `created_at`, `term_of_payment_id`, `time_to_payment`, `reference`, `quantity`, `product_price`, `VAT`, `total_price`, `note`, `purchase_order_id`) VALUES
(9, 'Hoá đơn 1', '2017-06-02 08:42:00', 1, '2017-06-02 08:42:00', '', 5, '698.00', '69.80', '767.80', '', 'PO010617940');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `inventory`
--

CREATE TABLE `inventory` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `address1` varchar(255) NOT NULL,
  `address2` varchar(255) CHARACTER SET utf32 COLLATE utf32_vietnamese_ci DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `district_id` int(11) DEFAULT NULL,
  `zipcode` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `inventory`
--

INSERT INTO `inventory` (`id`, `name`, `address1`, `address2`, `country_id`, `city_id`, `district_id`, `zipcode`) VALUES
(1, 'Chi nhánh mặc định', 'HCM', NULL, 1, 1, 1, '+84'),
(2, 'Chi nhánh GL', 'Gia Lai', '', 1, 1, 1, '');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product`
--

CREATE TABLE `product` (
  `id` varchar(255) CHARACTER SET utf8 COLLATE utf8_vietnamese_ci NOT NULL,
  `barcode` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_vietnamese_ci NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `tags` varchar(255) CHARACTER SET utf8 COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `created_price` double DEFAULT NULL,
  `input_price` double DEFAULT NULL,
  `wholesale_price` double DEFAULT NULL,
  `retail_price` double DEFAULT NULL,
  `inventory_id` int(11) DEFAULT NULL,
  `unit` varchar(255) CHARACTER SET utf8 COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `weight` double DEFAULT NULL,
  `weight_unit_id` int(11) DEFAULT NULL,
  `size` varchar(255) CHARACTER SET utf8 COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `status` varchar(255) CHARACTER SET utf8 COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `is_combo` int(11) DEFAULT NULL,
  `is_optional` int(11) DEFAULT NULL,
  `product_id` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `product`
--

INSERT INTO `product` (`id`, `barcode`, `name`, `category_id`, `brand_id`, `description`, `tags`, `created_price`, `input_price`, `wholesale_price`, `retail_price`, `inventory_id`, `unit`, `weight`, `weight_unit_id`, `size`, `created_at`, `status`, `is_combo`, `is_optional`, `product_id`) VALUES
('123123', 123123, '12312', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-05-17 14:54:34', NULL, NULL, NULL, NULL),
('12412412', 2147483647, 'Điện thoại Iphone 6s 64 - 124124124', NULL, NULL, '124124124', NULL, 2412414121, 41241, 241241241, 24142121, 1, '124124', 4124124, 3, NULL, '2017-05-18 08:31:04', '1', 0, 1, 'IP0001'),
('213123', 123213, 'Điện thoại Iphone 6s 64 - 213213213123', NULL, NULL, '', NULL, 0, 0, 0, 0, 1, '', NULL, 3, NULL, '2017-05-18 08:45:41', '1', 0, 1, 'IP0001'),
('213123123', 2147483647, 'Điện thoại Iphone 6s 64 - 23123123', NULL, NULL, '', NULL, 0, 0, 0, 0, 1, '', NULL, 3, NULL, '2017-05-18 08:43:57', '1', 0, 1, 'IP0001'),
('2342423423', 2147483647, 'Điện thoại Iphone 6s 64 - 234234', NULL, NULL, '', NULL, 0, 0, 0, 0, 1, '', NULL, 3, NULL, '2017-05-18 08:44:24', '1', 0, 1, 'IP0001'),
('312313', 2147483647, 'Điện thoại Iphone 6s 64 - 3123123123', NULL, NULL, '123123123123123', NULL, 123123123, 123123213, 231232131, 2313121, 1, '3123123', 2312312312, 3, NULL, '2017-05-18 08:29:40', '1', 0, 1, 'IP0001'),
('FY0001', 2147483647, 'FUck you..', NULL, NULL, '', NULL, 0, 0, 0, 0, 1, '', NULL, 3, NULL, '2017-05-18 08:47:51', '1', 1, 0, 'IP0001'),
('IP0001', 2147483647, 'Điện thoại Iphone 6s 64', 1, 4, 'Iphone 6s 64GB', 'hi', 20000000, 18000000, 21000000, 22000000, 1, 'cái', 19999, 3, 'Mặc định', '2017-05-17 13:40:10', '1', 0, 0, 'IP0001'),
('IP0002', 2147483647, 'Điện thoại Iphone 6s 64 - RED', NULL, NULL, 'helllo', NULL, 9, 1, 2, 3, 1, 'cái', 4, 3, NULL, '2017-05-17 14:05:17', '1', 0, 1, 'IP0001'),
('IP0003', 123, '123', NULL, NULL, '123', NULL, 0, 123, 213, 212313, 1, 'cái', 333, 3, NULL, '2017-05-17 14:31:06', '1', 1, 0, 'IP0001'),
('qweqweq', 213123123, 'Điện thoại Iphone 6s 64 - 123123123', NULL, NULL, '', NULL, 0, 231231, 123123, 0, 1, '', NULL, 3, NULL, '2017-05-18 08:31:30', '1', 1, 0, 'IP0001');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product_combo`
--

CREATE TABLE `product_combo` (
  `id` int(11) NOT NULL,
  `combo_id` varchar(255) NOT NULL,
  `product_id` varchar(255) NOT NULL,
  `quantity` int(11) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `total_price` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `product_combo`
--

INSERT INTO `product_combo` (`id`, `combo_id`, `product_id`, `quantity`, `price`, `total_price`) VALUES
(111, 'FY0001', '12412412', 5, NULL, NULL),
(112, 'FY0001', '312313', 2, NULL, NULL),
(113, 'FY0001', '312313', 3, NULL, NULL),
(114, 'FY0001', '312313', 3, NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product_import_bill`
--

CREATE TABLE `product_import_bill` (
  `product_id` varchar(255) NOT NULL,
  `import_bill_id` int(11) NOT NULL,
  `quantity` int(11) DEFAULT NULL,
  `rest` int(11) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `tax` double DEFAULT NULL,
  `total_price` decimal(10,2) DEFAULT NULL,
  `purchase_order_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `product_import_bill`
--

INSERT INTO `product_import_bill` (`product_id`, `import_bill_id`, `quantity`, `rest`, `total`, `tax`, `total_price`, `purchase_order_id`) VALUES
('12412412', 9, 3, 2, 5, 10, '300.00', 'PO010617940'),
('213123123', 9, 2, 4, 6, 10, '398.00', 'PO010617940');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product_inventory`
--

CREATE TABLE `product_inventory` (
  `product_id` varchar(255) NOT NULL,
  `inventory_id` int(11) NOT NULL,
  `quantity` int(11) DEFAULT NULL,
  `being_traded` int(11) DEFAULT NULL,
  `can_sale` int(11) DEFAULT NULL,
  `being_coming` int(11) DEFAULT NULL,
  `minimum_existence` int(11) DEFAULT NULL,
  `maximum_existence` int(11) DEFAULT NULL,
  `position` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `product_inventory`
--

INSERT INTO `product_inventory` (`product_id`, `inventory_id`, `quantity`, `being_traded`, `can_sale`, `being_coming`, `minimum_existence`, `maximum_existence`, `position`) VALUES
('213123123', 1, NULL, 0, 0, 0, 0, 0, NULL),
('312313', 1, 324234, 0, 324234, 0, 0, 0, NULL),
('IP0001', 1, 69, 0, 69, 0, 0, 0, NULL),
('IP0002', 1, 96, 0, 96, 0, 0, 0, NULL),
('wqeqwe', 1, NULL, 0, 0, 0, 0, 0, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `product_purchase_order`
--

CREATE TABLE `product_purchase_order` (
  `product_id` varchar(255) NOT NULL,
  `purchase_order_id` varchar(255) NOT NULL,
  `quantity` int(11) DEFAULT NULL,
  `price` double(10,2) DEFAULT NULL,
  `tax` double DEFAULT NULL,
  `total_price` double(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `product_purchase_order`
--

INSERT INTO `product_purchase_order` (`product_id`, `purchase_order_id`, `quantity`, `price`, `tax`, `total_price`) VALUES
('12412412', 'PO010617940', 5, 100.00, 10, 500.00),
('12412412', 'PO310517215', 5, 5000.00, 10, 25000.00),
('213123', 'PO310517935', 1, 500.00, 10, 500.00),
('213123123', 'PO010617940', 6, 199.00, 10, 1200.00),
('213123123', 'PO310517215', 5, 3999.00, 10, 19995.00),
('213123123', 'PO310517935', 1, 600.00, 10, 600.00);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `purchase_order`
--

CREATE TABLE `purchase_order` (
  `id` varchar(255) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `address_supplier_id` int(11) DEFAULT NULL,
  `phone_number` int(11) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `delivery_date` datetime DEFAULT NULL,
  `inventory_id` int(11) DEFAULT NULL,
  `group_tax_id` int(11) DEFAULT NULL,
  `group_price_id` int(11) DEFAULT NULL,
  `tags` varchar(255) CHARACTER SET utf8 COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `note` varchar(255) CHARACTER SET utf8 COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `product_price` double DEFAULT NULL,
  `VAT` double DEFAULT NULL,
  `total_price` double DEFAULT NULL,
  `status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `purchase_order`
--

INSERT INTO `purchase_order` (`id`, `supplier_id`, `address_supplier_id`, `phone_number`, `email`, `delivery_date`, `inventory_id`, `group_tax_id`, `group_price_id`, `tags`, `note`, `quantity`, `product_price`, `VAT`, `total_price`, `status`) VALUES
('PO010617940', 0, 14, 258456123, 'asus@gmail.com', '2017-06-03 10:40:00', 1, 0, 0, '', '', 11, 1700, 170, 1870, 0),
('PO310517215', 0, 14, 258456123, 'asus@gmail.com', '2017-06-03 11:40:00', 1, 0, 0, '', '', 10, 44995, 4499.5, 49494.5, 0),
('PO310517935', 0, 15, NULL, '', '2017-06-08 14:00:00', 1, 0, 0, '', '', 2, 1100, 110, 1210, 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `supplier`
--

CREATE TABLE `supplier` (
  `id` varchar(255) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_vietnamese_ci NOT NULL,
  `group_supplier_id` int(11) DEFAULT NULL,
  `phone_number` int(11) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8 COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `fax_number` int(11) DEFAULT NULL,
  `tax_number` int(11) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `tags` varchar(255) CHARACTER SET utf8 COLLATE utf8_vietnamese_ci DEFAULT NULL,
  `address_id` int(11) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `group_price_id` varchar(255) DEFAULT NULL,
  `group_tax_id` varchar(255) DEFAULT NULL,
  `term_of_payment_id` int(11) DEFAULT NULL,
  `group_payment_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `supplier`
--

INSERT INTO `supplier` (`id`, `name`, `group_supplier_id`, `phone_number`, `email`, `description`, `fax_number`, `tax_number`, `website`, `tags`, `address_id`, `employee_id`, `group_price_id`, `group_tax_id`, `term_of_payment_id`, `group_payment_id`) VALUES
('SUP310517603', 'NOKIA', NULL, NULL, '', '', NULL, NULL, '', '', NULL, 4, 'BANBUON', 'BANHANG', 1, 1),
('SUP31051786', 'ASUS', NULL, 258456123, 'asus@gmail.com', '', NULL, NULL, '', '', NULL, 4, 'BANBUON', 'BANHANG', 1, 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tax`
--

CREATE TABLE `tax` (
  `id` int(11) NOT NULL,
  `name` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `code` varchar(256) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `tax_rate` float DEFAULT NULL,
  `is_default` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `tax`
--

INSERT INTO `tax` (`id`, `name`, `code`, `tax_rate`, `is_default`) VALUES
(1, 'Thuế nhập hàng', 'NHAPHANG', 10, 1),
(2, 'Không áp dụng thuế', 'KHONGAPDUNG', 0, 2),
(3, 'Thuế bán hàng', 'BANHANG', 10, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `term_of_payment`
--

CREATE TABLE `term_of_payment` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_vietnamese_ci NOT NULL,
  `days` int(11) DEFAULT NULL,
  `from` int(11) DEFAULT NULL,
  `is_default` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `term_of_payment`
--

INSERT INTO `term_of_payment` (`id`, `name`, `days`, `from`, `is_default`) VALUES
(1, 'Thanh toán ngay', 0, 0, 1),
(2, 'Thanh toán trong 1 tháng', 30, 0, 0),
(3, 'Thanh toán trong 1 tuần', 7, 0, 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `weight_unit`
--

CREATE TABLE `weight_unit` (
  `id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `weight_unit`
--

INSERT INTO `weight_unit` (`id`, `name`) VALUES
(1, 'lb'),
(2, 'oz'),
(3, 'kg'),
(4, 'g');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `address_supplier`
--
ALTER TABLE `address_supplier`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `brand`
--
ALTER TABLE `brand`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `currency`
--
ALTER TABLE `currency`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `customer_address`
--
ALTER TABLE `customer_address`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `customer_contact`
--
ALTER TABLE `customer_contact`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `customer_default_setting`
--
ALTER TABLE `customer_default_setting`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `customer_note`
--
ALTER TABLE `customer_note`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `district`
--
ALTER TABLE `district`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `group_customer`
--
ALTER TABLE `group_customer`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `group_payment`
--
ALTER TABLE `group_payment`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `group_price`
--
ALTER TABLE `group_price`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `group_supplier`
--
ALTER TABLE `group_supplier`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `group_tax`
--
ALTER TABLE `group_tax`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `import_bill`
--
ALTER TABLE `import_bill`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `inventory`
--
ALTER TABLE `inventory`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `product_combo`
--
ALTER TABLE `product_combo`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `product_import_bill`
--
ALTER TABLE `product_import_bill`
  ADD PRIMARY KEY (`product_id`,`import_bill_id`);

--
-- Chỉ mục cho bảng `product_inventory`
--
ALTER TABLE `product_inventory`
  ADD PRIMARY KEY (`product_id`,`inventory_id`);

--
-- Chỉ mục cho bảng `product_purchase_order`
--
ALTER TABLE `product_purchase_order`
  ADD PRIMARY KEY (`product_id`,`purchase_order_id`);

--
-- Chỉ mục cho bảng `purchase_order`
--
ALTER TABLE `purchase_order`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tax`
--
ALTER TABLE `tax`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `term_of_payment`
--
ALTER TABLE `term_of_payment`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `weight_unit`
--
ALTER TABLE `weight_unit`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `address_supplier`
--
ALTER TABLE `address_supplier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT cho bảng `brand`
--
ALTER TABLE `brand`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT cho bảng `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT cho bảng `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT cho bảng `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT cho bảng `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT cho bảng `customer_address`
--
ALTER TABLE `customer_address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT cho bảng `customer_contact`
--
ALTER TABLE `customer_contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT cho bảng `customer_default_setting`
--
ALTER TABLE `customer_default_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT cho bảng `customer_note`
--
ALTER TABLE `customer_note`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT cho bảng `district`
--
ALTER TABLE `district`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT cho bảng `employee`
--
ALTER TABLE `employee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT cho bảng `group_customer`
--
ALTER TABLE `group_customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT cho bảng `group_payment`
--
ALTER TABLE `group_payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT cho bảng `group_supplier`
--
ALTER TABLE `group_supplier`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT cho bảng `import_bill`
--
ALTER TABLE `import_bill`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT cho bảng `inventory`
--
ALTER TABLE `inventory`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT cho bảng `product_combo`
--
ALTER TABLE `product_combo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=115;
--
-- AUTO_INCREMENT cho bảng `tax`
--
ALTER TABLE `tax`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT cho bảng `term_of_payment`
--
ALTER TABLE `term_of_payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT cho bảng `weight_unit`
--
ALTER TABLE `weight_unit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
