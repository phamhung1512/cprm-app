(function( $ ) {
/**
 * START - ONLOAD - JS
 */
/* ----------------------------------------------- */
/* ------------- FrontEnd Functions -------------- */
/* ----------------------------------------------- */

// 1. Show table filter
function showTableFilter() {
    $('.filt-btn').on('click', function(e) {
        if($(this).hasClass('expand')) {
            $(this).removeClass('expand');
            $(this).closest('.tb-filter-inner').siblings('.tb-filter-expand').removeClass('shw');
        } else {
            $(this).addClass('expand');
            $(this).closest('.tb-filter-inner').siblings('.tb-filter-expand').addClass('shw');
        }
    });
}

// 2. Show dashboard head func
function showDashboardHead() {
    $('.d-func-btn').each(function() {
        $(this).on('click', function(e){
            if($(this).hasClass('active')) {
                $(this).removeClass('active');
                $(this).siblings('.sub-list').removeClass('shw');
            } else {
                $('.d-func-btn').removeClass('active');
                $(this).addClass('active');
                $('.d-func-btn').siblings('.sub-list').removeClass('shw');
                $(this).siblings('.sub-list').addClass('shw');
            }
        });
    });
}

// 3. Main menu
function handleMainMenu() {
    $('.m-itm').each(function() {
        $(this).on('click', function(e) {
            if($(this).hasClass('active')) {
                $(this).removeClass('active');
                $(this).siblings('.sub-menu').removeClass('shw');
            } else {
                $('.m-itm').removeClass('active');
                $('.m-itm').siblings('.sub-menu').removeClass('shw');
    
                $(this).addClass('active');
                $(this).siblings('.sub-menu').addClass('shw');
            }

        });
    });

    // Submenu click
    $('.sub-menu-itm').each(function() {
        $(this).on('click', function(e) {
            $('.sub-menu-itm').removeClass('active');
            $(this).addClass('active');
        });
    });
}

// 4. Perfect scrollbar
function perfectScrollBar(className, option) {
    var ps = new PerfectScrollbar(className, option);
}

// 5. User detail show log work
function showLogWork() {
    $('.shw-log-wrk').each(function() {
        $(this).on('click', function(e) {
            var id = $(this).attr('data-id');
            if($(this).hasClass('active')) {
                $(this).removeClass('active');
                $(this).siblings('#' + id).removeClass('shw');
            } else {
                $(this).addClass('active');
                $(this).siblings('#' + id).addClass('shw');
            }
            e.preventDefault();
        });
    });
}


/* ----------------------------------------------- */
/* ----------------------------------------------- */
/* OnLoad Page */
$(document).ready(function($){
    showTableFilter();
    showDashboardHead();
    handleMainMenu();
    showLogWork();

    // Perfect scrollbar
    perfectScrollBar('.noti-lst');
    perfectScrollBar('.main-menu');
    if($('.log-wrk-list').length) {
        perfectScrollBar('.log-wrk-list');
        perfectScrollBar('#log-order-list');
    }

    if($('.access-list').length) {
        perfectScrollBar('.access-list');
    }
    perfectScrollBar('.body-wrap');
    perfectScrollBar('.prov-contract');

    // Light gallery
    $("#lightgallery").lightGallery(); 

    
});
/* OnLoad Window */
var init = function () {

};
window.onload = init;

})(jQuery);
