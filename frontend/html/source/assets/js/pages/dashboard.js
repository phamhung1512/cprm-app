/**
 * JS PAGE TEAM
 * ------------------------
 * 1. Draw dashboard chart
 */
var dashboard_fn = {};
(function( $ ) { 
/**
 * 1. Draw dashboard chart
 */

dashboard_fn.drawChart = function(id, options){
    AmCharts.makeChart(id, options);
};



/* OnLoad Page */
$(document).ready(function($){
    revenue_options = {
        "type": "serial",
        "categoryField": "category",
        "colors": [
            "#0063d1",
            "#e83e8c"
        ],
        "startDuration": 1,
        "startEffect": "easeOutSine",
        "usePrefixes": true,
        "categoryAxis": {
            "classNameField": "",
            "gridPosition": "start",
            "minPeriod": "MM",
            "boldLabels": true,
            "title": "",
            "titleFontSize": 1
        },
        "chartCursor": {
            "enabled": true,
            "cursorColor": "#009DA0",
            "valueLineBalloonEnabled": true,
            "valueLineEnabled": true
        },
        "trendLines": [],
        "graphs": [
            {
                "balloonText": "[[title]] of [[category]]:[[value]]",
                "fillAlphas": 1,
                "id": "AmGraph-1",
                "title": "graph 1",
                "type": "column",
                "valueField": "column-1"
            },
            {
                "balloonText": "[[title]] of [[category]]:[[value]]",
                "bullet": "round",
                "id": "AmGraph-2",
                "lineThickness": 2,
                "title": "graph 2",
                "type": "smoothedLine",
                "valueField": "column-2"
            }
        ],
        "guides": [],
        "valueAxes": [
            {
                "axisFrequency": 10,
                "id": "ValueAxis-1",
                "integersOnly": true,
                "minimum": 0,
                "precision": 2,
                "unit": "$",
                "unitPosition": "left",
                "usePrefixes": true,
                "useScientificNotation": true,
                "title": ""
            }
        ],
        "allLabels": [],
        "balloon": {},
        "legend": {
            "enabled": true,
            "useGraphSettings": true
        },
        "titles": [
            {
                "id": "Title-1",
                "size": 15,
                "text": ""
            }
        ],
        "dataProvider": [
            {
                "category": "Jan",
                "column-1": "80",
                "column-2": "55"
            },
            {
                "category": "Feb",
                "column-1": "60",
                "column-2": "87"
            },
            {
                "category": "Mar",
                "column-1": "25.6",
                "column-2": "54.45"
            },
            {
                "category": "Apr",
                "column-1": "47.3",
                "column-2": "44"
            },
            {
                "category": "May",
                "column-1": "54",
                "column-2": "24"
            },
            {
                "category": "Jun",
                "column-1": "102.33",
                "column-2": "68"
            },
            {
                "category": "Jul",
                "column-1": "20",
                "column-2": "92.5"
            },
            {
                "category": "Aug",
                "column-1": "45",
                "column-2": "44"
            },
            {
                "category": "Sep",
                "column-1": "77",
                "column-2": "22"
            },
            {
                "category": "Oct",
                "column-1": "68",
                "column-2": "76"
            },
            {
                "category": "Nov",
                "column-1": "201",
                "column-2": "120"
            },
            {
                "category": "Dec",
                "column-1": "103",
                "column-2": "24"
            }
        ]
    };

    best_sell = {
        "type": "serial",
        "categoryField": "category",
        "colors": [
            "#0063d1"
        ],
        "startDuration": 1,
        "startEffect": "easeOutSine",
        "categoryAxis": {
            "gridPosition": "start",
            "boldLabels": true
        },
        "chartCursor": {
            "enabled": true,
            "cursorColor": "#009DA0",
            "valueLineBalloonEnabled": true,
            "valueLineEnabled": true
        },
        "trendLines": [],
        "graphs": [
            {
                "balloonText": "[[title]] of [[category]]:[[value]]",
                "fillAlphas": 1,
                "id": "best-sell-1",
                "title": "graph 1",
                "type": "column",
                "valueField": "column-1"
            }
        ],
        "guides": [],
        "valueAxes": [
            {
                "minimum": 0,
                "id": "ValueAxis-1",
                "title": ""
            }
        ],
        "allLabels": [],
        "balloon": {},
        "legend": {
            "enabled": true,
            "useGraphSettings": true
        },
        "titles": [
            {
                "id": "Title-1",
                "size": 15,
                "text": ""
            }
        ],
        "dataProvider": [
            {
                "category": "Jan",
                "column-1": 8
            },
            {
                "category": "Feb",
                "column-1": 6
            },
            {
                "category": "Mar",
                "column-1": 2
            },
            {
                "category": "Apr",
                "column-1": null
            },
            {
                "category": "May",
                "column-1": null
            },
            {
                "category": "Jun",
                "column-1": null
            },
            {
                "category": "Jul",
                "column-1": null
            },
            {
                "category": "Aug",
                "column-1": null
            },
            {
                "category": "Sep",
                "column-1": null
            },
            {
                "category": "Oct",
                "column-1": null
            },
            {
                "category": "Nov",
                "column-1": null
            },
            {
                "category": "Dec",
                "column-1": null
            }
        ]
    };

    dashboard_fn.drawChart('revenue-chrt', revenue_options);
    dashboard_fn.drawChart('best-sell-prod-chrt', best_sell);
});
/* OnLoad Window */
var init = function () {   

};
window.onload = init;
})(jQuery);
