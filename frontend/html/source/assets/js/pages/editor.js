/**
 * JS PAGE TEAM
 * ------------------------
 * 1. Draw quill editor
 */
var editor_fn = {};
(function( $ ) { 
/**
 * 1. Draw quill editor
 */
var toolbarOptions = [
    ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
    ['blockquote', 'code-block'],
  
    [{ 'header': 1 }, { 'header': 2 }],               // custom button values
    [{ 'list': 'ordered'}, { 'list': 'bullet' }],
    [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
    [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
    [{ 'direction': 'rtl' }],                         // text direction
  
    [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
    [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
    [ 'link', 'image', 'video', 'formula' ],
  
    [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
    [{ 'font': [] }],
    [{ 'align': [] }],
  
    ['clean']                                         // remove formatting button
];
var options = {
    modules: {
        toolbar: toolbarOptions
    },
    theme: 'snow'
}

var new_options = {
    placeholder: 'Enter description',
    modules: {
        toolbar: toolbarOptions
    },
    theme: 'snow'
}

editor_fn.createEditor = function(id, options) {
    var quill = new Quill(id, options);
}


/* OnLoad Page */
$(document).ready(function($){
    editor_fn.createEditor('#editor-txt-1', options);
    editor_fn.createEditor('#editor-txt-2', options);
    editor_fn.createEditor('#new-editor-txt-1', new_options);
    editor_fn.createEditor('#new-editor-txt-2', new_options);
});
/* OnLoad Window */
var init = function () {   

};
window.onload = init;
})(jQuery);
