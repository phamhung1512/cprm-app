/**
 * JS PAGE TEAM
 * ------------------------
 * 1. Draw dashboard chart
 */
var product_fn = {};
(function( $ ) { 
/**
 * 1. Draw dashboard chart
 */

product_fn.drawChart = function(id, options){
    AmCharts.makeChart(id, options);
};



/* OnLoad Page */
$(document).ready(function($){
    revenue_options = {
        "type": "serial",
        "categoryField": "category",
        "colors": [
            "#0063d1",
            "#e83e8c"
        ],
        "startDuration": 1,
        "startEffect": "easeOutSine",
        "usePrefixes": true,
        "categoryAxis": {
            "classNameField": "",
            "gridPosition": "start",
            "minPeriod": "MM",
            "boldLabels": true,
            "title": "",
            "titleFontSize": 1
        },
        "chartCursor": {
            "enabled": true,
            "cursorColor": "#009DA0",
            "valueLineBalloonEnabled": true,
            "valueLineEnabled": true
        },
        "trendLines": [],
        "graphs": [
            {
                "balloonText": "[[title]] of [[category]]:[[value]]",
                "fillAlphas": 1,
                "id": "AmGraph-prod-1",
                "title": "graph 1",
                "type": "column",
                "valueField": "column-1"
            },
            {
                "balloonText": "[[title]] of [[category]]:[[value]]",
                "bullet": "round",
                "id": "AmGraph-prod-2",
                "lineThickness": 2,
                "title": "graph 2",
                "type": "smoothedLine",
                "valueField": "column-2"
            }
        ],
        "guides": [],
        "valueAxes": [
            {
                "axisFrequency": 10,
                "id": "ValueAxis-prod-1",
                "integersOnly": true,
                "minimum": 0,
                "precision": 2,
                "unit": "$",
                "unitPosition": "left",
                "usePrefixes": true,
                "useScientificNotation": true,
                "title": ""
            }
        ],
        "allLabels": [],
        "balloon": {},
        "legend": {
            "enabled": true,
            "useGraphSettings": true
        },
        "titles": [
            {
                "id": "Title-prod-1",
                "size": 15,
                "text": "Product sell statistic"
            }
        ],
        "dataProvider": [
            {
                "category": "Jan",
                "column-1": "80",
                "column-2": "55"
            },
            {
                "category": "Feb",
                "column-1": "60",
                "column-2": "87"
            },
            {
                "category": "Mar",
                "column-1": "25.6",
                "column-2": "54.45"
            },
            {
                "category": "Apr",
                "column-1": "47.3",
                "column-2": "44"
            },
            {
                "category": "May",
                "column-1": "54",
                "column-2": "24"
            },
            {
                "category": "Jun",
                "column-1": "102.33",
                "column-2": "68"
            },
            {
                "category": "Jul",
                "column-1": "20",
                "column-2": "92.5"
            },
            {
                "category": "Aug",
                "column-1": "45",
                "column-2": "44"
            },
            {
                "category": "Sep",
                "column-1": "77",
                "column-2": "22"
            },
            {
                "category": "Oct",
                "column-1": "68",
                "column-2": "76"
            },
            {
                "category": "Nov",
                "column-1": "201",
                "column-2": "120"
            },
            {
                "category": "Dec",
                "column-1": "103",
                "column-2": "24"
            }
        ]
    };

    var pie_options = {
        "type": "pie",
        "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
        "colors": [
            "#e83e8c",
            "#0063d1",
            "#EE5838"
        ],
        "pullOutEffect": "easeOutSine",
        "startEffect": "easeOutSine",
        "titleField": "category",
        "valueField": "column-1",
        "usePrefixes": true,
        "allLabels": [],
        // "balloon": {},
        "legend": {
            "enabled": true,
            "align": "center",
            "markerType": "circle"
        },
        "titles": [
            {
                "id": "Title-2",
                "size": 15,
                "text": "Product area sell chart"
            }
        ],
        "dataProvider": [
            {
                "category": "category 1",
                "column-1": 8
            },
            {
                "category": "category 2",
                "column-1": 6
            },
            {
                "category": "category 3",
                "column-1": 2
            }
        ]
    }

    product_fn.drawChart('prod-sell-chrt', revenue_options);
    product_fn.drawChart('area-sell-chrt', pie_options);
});
/* OnLoad Window */
var init = function () {   

};
window.onload = init;
})(jQuery);
